import random
import time

ap_x = [15, 80, 50]
ap_y = [10, 0, 80]

def mockdata(length):
    d = {}
    for i in range(length):
        d[i] = {'x':random.randint(0,100), 'y':random.randint(0,100), 'time':time.time()}
    return d
