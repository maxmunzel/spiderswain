from bokeh.models import sources
from bokeh.plotting import figure, output_file, show
from mock import ap_x, ap_y, mockdata
import struct
import time

output_file("spider.html")

def time_to_hex(unixtime):
    return hex(struct.unpack('<I', struct.pack('<f', unixtime))[0])

if __name__ == "__main__":
    p = figure(plot_width=1000, plot_height=600)

    # Initiate AP Positions
    p.circle_cross(ap_x, ap_y, fill_alpha=0, line_width=3,
                  color="#FB8072", size=25)

    d = mockdata(15)
    ds = sources.ColumnDataSource(d)
    for key in ds.data.keys():
        p.circle(x=d[key]['x'], y= d[key]['y'], size=22, color='#'+time_to_hex(time.time())[-6:])
    show(p)
