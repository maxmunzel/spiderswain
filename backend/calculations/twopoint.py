"""
Calculates the two intersection points of two overlapping circles. One of it is the
approximate position of the phone, the other one is irrelevant. The distinction
is performed outside of this function.
"""

import numpy as np
import math


def rotate(invector, angle):
    """
    Rotates a given 2-D numpy vector a given radians
    """

    invector = np.array(invector).reshape(2, 1)
    # create a 2x2 rotation matrix based on the given angle
    rotmatrix = np.matrix([[math.cos(angle), -math.sin(angle)],
                           [math.sin(angle), math.cos(angle)]]

                          )
    # calculate rotated vector by dot multiplication and return it
    return np.dot(rotmatrix, invector)


def get_possible_locations(AP1, AP2, ap1_p, ap2_p):
    # small letters -> distances ; BIG letters --> Vectors of Antennas

    # convert locations of antennas to np arrays to use em as vectors
    AP1 = np.array(AP1)
    AP2 = np.array(AP2)

    # create a unified vector between the two APs
    helping_vector = np.array(AP2 - AP1)
    distance_between_aps = abs(np.linalg.norm(helping_vector))
    helping_vector = helping_vector / distance_between_aps


    # if distances are to short to actually meet somewhere, enlarge them repetively until they meet somewhere
    # this can happen because of slightly false numbers when the point is in the middle of both APs
    while (ap1_p + ap2_p) < distance_between_aps:
        ap1_p *= 1.05
        ap2_p += 1.05
    #calculate the angle (alpha) we have to rotate helping_vector in order to reach P1
    alpha = math.acos((ap2_p ** 2 - ap1_p ** 2 - distance_between_aps ** 2) /
                      (-2 * ap1_p * distance_between_aps))

    # strech helping_vetor to the length between AP1 and P
    helping_vector = helping_vector * ap1_p

    # rotate helping_vector +/- alpha to reach both possible points
    AP1_P_1 = np.array(rotate(helping_vector, alpha))
    AP1_P_2 = np.array(rotate(helping_vector, -alpha))
    # add AP1s location to form the location vectors of P1 P2
    #AP1_P_1 = np.add(AP1_P_1, AP1)
    #AP1_P_2 = np.add(AP1_P_2, AP1)
    AP1_P_1 = np.array(((float(AP1_P_1[0])+float(AP1[0])),
                       (float(AP1_P_1[1])+float(AP1[1]))))
    AP1_P_2 = np.array(((float(AP1_P_2[0])+float(AP1[0])),
                       (float(AP1_P_2[1])+float(AP1[1]))))
    # swap x and y, whyever they get messed up at this point
    result = [np.dot(np.matrix([[1, 0], [0, 1]]), AP1_P_1),
              np.dot(np.matrix([[1, 0], [0, 1]]), AP1_P_2)]
    result = [AP1_P_1,AP1_P_2]
    # bring the vectors in the form of points
    result[0] = result[0].reshape(1, 2)
    result[1] = result[1].reshape(1, 2)
    return result


if __name__ == "__main__":
    for result in (get_possible_locations((0, 8),
                                          (0, 0),
                                          6.4,
                                          5)):
        print(result)
