#!/usr/bin/python
from librarys.kafka_handler import Consumer
from librarys.FrameHandler import FrameHandler

class Handler():

	def handle(self, data):
		data = self.clean(data)
		FrameHandler.add(data)

	def clean(self, data):
		data = data.value
		data = data.decode('UTF-8')
		data = data.split(' ')
		return data

if __name__ == '__main__':
	Handler = Handler()
	FrameHandler = FrameHandler()
	Consumer = Consumer('localhost:9092', 'test', Handler.handle)