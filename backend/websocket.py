import asyncio
import websockets
import json
import random

"""
Establishes an websocket server to transfer json data for visualisation purposes
"""

@asyncio.coroutine
def main(websocket, path):
    while True:
        message = yield from websocket.recv()
        yield from websocket.send(message)

        yield from asyncio.sleep(0.03)
start_server = websockets.serve(main, '0.0.0.0', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

