import sqlite3

conn = sqlite3.connect("spots.db")
c = conn.cursor()


def get_key():
    c.execute("SELECT max(key) FROM spots ")

    key = c.fetchone()
    if key == (None,):
        return 0
    return key[0] + 1


def insert(x, y, val1, val2, val3):
    c.execute("INSERT INTO spots (key, x, y, val1, val2, val3) values (?, ?, ?, ?, ?, ?)",
              (get_key(), x, y, val1, val2, val3))
    conn.commit()


def get_locations():
    c.execute("SELECT DISTINCT x, y from spots")
    return c.fetchall()


def get_vals(position):
    """Fetches the average Values for a position. The position is given as a tuple of size 2 (x, y)
    Returns tuple of size 3."""
    c.execute("SELECT val1, val2, val3 from spots WHERE x = (?) AND y = (?)", position)
    vals = c.fetchall()
    alpha = 0.0
    beta = 0.0
    charlie = 0.0
    for val in vals:
        alpha += val[0]
        beta += val[1]
        charlie += val[2]
    alpha /= len(vals)
    beta /= len(vals)
    charlie /= len(vals)
    return (alpha, beta, charlie)


def get_best_spot(val1, val2, val3):
    def score(pair):
        coordinates = pair[1]
        delta1 = abs(coordinates[0] - val1)
        delta2 = abs(coordinates[1] - val2)
        delta3 = abs(coordinates[2] - val3)
        return delta1 + delta2 + delta3

    pairs = []
    vals = []
    for location in get_locations():
        vals = get_vals(location)

        pairs.append((location, vals))

    return sorted(pairs, key=score)[0][0]


if __name__ == "__main__":
    while True:
        print(eval(input(">")))
