from librarys.math import do_it

class Frame(object):

	def __init__(self, ident):
		self.id = ident

		self.frames = dict()
	
	def add(self, package):
		t = int(package[0])
		self.frames[t] = {
			'from': package[0],
			'macs': package[2],
			'dbm': package[3]
		}

		self.self_check()

	def self_check(self):
		if len(self.frames) == 3:
			#print('Got 3 data packages')
			#print(self.frames)
			#print('WUPWUP')
			pos = do_it(int(self.frames[1]['dbm']),
						int(self.frames[2]['dbm']),
						int(self.frames[3]['dbm']))
			print(self.frames[1]['macs']+': '+str(pos))
			#print(str(self.frames[1]['macs'][1])+": "+str(do_it(100+int(self.frames[1]['dbm']), 100+int(self.frames[2]['dbm']), 100+int(self.frames[3]['dbm']))))

class FrameHandler(object):

	def __init__(self):
		self.frames = dict()

	def add(self, frame):
		frame_id = frame[1]

		if not frame_id in self.frames:
			self.frames[frame_id] = Frame(frame_id)
		self.frames[frame_id].add(frame)