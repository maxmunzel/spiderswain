from kafka import KafkaConsumer

class ConsumerException(Exception):
	def __init__(self, exception):
		super(ConsumerException, self).__init__(exception)
		self.message = exception

class Consumer(object):

	def __init__(self, addr, topic, handle_function, debug=False):
		self.addr = addr
		self.topic = topic
		self.handle_function = handle_function
		self.debug = debug

		self.consumer = None

		self.connect()
		self.handle()

	def connect(self):
		try:
			self.consumer = KafkaConsumer(self.topic, bootstrap_servers=[self.addr])
		except Exception as e:
			raise ConsumerException('Could not connect to kafka')
			return 0
		else:
			return 1

	def handle(self):
		for message in self.consumer:
			if self.debug:
				print('Knock knock')
			message = message
			try:
				self.handle_function(message)
			except Exception as e:
				raise ConsumerException('I called the wrong function, and at this point i am to afraid to ask for the right one')
			else:
				if self.debug:
					print('Fertig gehandelt')
			finally:
				self.consumer.task_done(message)