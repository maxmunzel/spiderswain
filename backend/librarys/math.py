import numpy as np
from calculations import calcpos
from calculations import distance

ds = distance.distance

P1 = np.array([11, -4])
P2 = np.array([0, 0])
P3 = np.array([5, 7])


def do_it(p1, p2, p3):
	print(str(ds(p1/10))+"\t"+str(p1/10))
	print(str(ds(p2/10))+"\t"+str(p2/10))
	print(str(ds(p3/10))+"\t"+str(p3/10))
	r = calcpos.calcpos(P1, P2, P3, ds(p1)/10, ds(p2)/10, ds(p3)/10)
	return r