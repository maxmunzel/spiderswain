__author__ = 'gregor'

from hashlib import md5
from sys import argv, exit

from librarys.sharknado import Shark

IFACE = str()
WID = int()

def run(IFACE):
    Animal = Shark(IFACE)
    Animal.hunt()

class Package(object):

    def __init__(self, stype, seq, ass, mac):
        self.__dict__['id'] = None

        self.__dict__['stype'] = hex(stype).encode('UTF-8')
        self.__dict__['seq'] = hex(seq).encode('UTF-8')
        self.__dict__['ass'] = ass.hex().encode('UTF-8')
        self.__dict__['mac'] = mac.encode('UTF-8')

        self.anonym()
        self.ident()

    def anonym(self):
        mac = md5()
        mac.update(self.__dict__['mac'])
        self.__dict__['mac'] = mac.digest()

    def ident(self):
        ident = md5()
        ident.update(self.__dict__['stype'])
        ident.update(self.__dict__['seq'])
        ident.update(self.__dict__['mac'])

        self.__dict__['id'] = ident.digest()

    def dump(self):
        return self.__dict__

if __name__ == '__main__':
    if len(argv) == 3:
        IFACE = argv[1]
        WID = argv[2]

        run(IFACE)
    else:
        print('These are the command-line options you are looking for:')
        print(' monitor-interface')
        print(' workerswain-id')

        exit()