__author__ = 'gregor'

import scapy.all
import librarys.scapy_ex

class SharkException(Exception):
    """
        Excpetion for errors in the Shark
    """
    def __init__(self, message):
        self.message = message

class Shark(object):
    """
        Object that representates the scapy
        sniffing
    """
    def __init__(self, iface):
        self.iface = iface

    def hunt(self):
        try:
            scapy.all.sniff(iface=self.iface, prn=self.handle)
        except Exception as e:
            print('It just felt off: '+str(e))

    def handle(self, package):
        try:
            dbm = package.getfieldval('dBm_AntSignal')+100

            package = package.payload
            if package.type == 0:
                if 'SC' in package.fields:
                    seq = package.getfieldval('SC')
                    hw = package.getfieldval('addr2')
                    print(str(dbm)+','+str(seq)+','+str(hw))
                else:
                    print('keine SC ? .-.')
            else:
                #print('Heute leider nein')
                pass
        except Exception as e:
            print(str(e))