__author__ = 'gregor'

from hashlib import md5

class PackageException(Exception):
    """
        Exception for error in the Package
    """
    def __init__(self, message):
        self.message = message

class Package(object):

    __dict__ = dict()

    def __init__(self, stype, seq, ass, mac):
        self.__dict__['id'] = None

        self.__dict__['stype'] = hex(stype).encode('UTF-8')
        self.__dict__['seq'] = hex(seq).encode('UTF-8')
        self.__dict__['ass'] = ass.hex().encode('UTF-8')
        self.__dict__['mac'] = mac.encode('UTF-8')

        self.anonym()
        self.ident()

    def anonym(self):
        mac = md5()
        mac.update(self.__dict__['mac'])
        self.__dict__['mac'] = mac.digest()

    def ident(self):
        ident = md5()
        ident.update(self.__dict__['stype'])
        ident.update(self.__dict__['seq'])
        ident.update(self.__dict__['mac'])

        self.__dict__['id'] = ident.digest()

    def dump(self):
        return self.__dict__
