__author__ = 'Max Nagy'


def distance(signal_strength_in_db, frequency=2412, unit_constant=27.55):

    """
    Calculates the distance between two wireless devices within line-of-sight based on signal strength.
    Math based on:
     http://stackoverflow.com/questions/11217674/how-to-calculate-distance-from-wifi-router-using-signal-strength
    As it is based on free-space path loss, it doesn't work well in close distance to the emitter because of reflections

    :param signal_strength_in_db: float
        signal strength in Db
    :param frequency: int
        frequency of the wifi. Standard is 2412, adjust when needed (eg when using 5GHz).
        unit is MHz, unless you change unit_constant
    :param unit_constant: float
        conversion constant, if unchanged distance is in meters, frequency in MHz
    :return: float
        distance. (in meters, unless you adjust unit_constant)
    """
    import math
    # make sure, signal_strength_in_db is always negative.
    signal_strength_in_db = abs(signal_strength_in_db)
    # return pow(10, (unit_constant-((20 * math.log10(frequency))+signal_strength_in_db)))
    return 10 ** ((unit_constant - (20 * math.log10(frequency)) + signal_strength_in_db)/20)
if __name__ == "__main__":
    while True:
        print(distance(int(input(">"))))

