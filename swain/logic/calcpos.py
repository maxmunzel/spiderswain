import twopoint
import numpy as np


def calcpos(AP1, AP2, AP3, ap1_p, ap2_p, ap3_p):
    """Calculates a score for each possible location to find the best Canidate based on proximity"""
    def score(spot):
        def in_reach(spot1, spot2, radius):
            connection = np.array(spot2-spot1)
            return radius > np.linalg.norm(connection)
        radius = 0
        while True:
            n = 0
            for s in spots:
                if in_reach(s, spot, radius):
                    n += 1
            if n > 1:
                return radius
            else:
                radius += 0.1


    spots = list()
    # add all 6 possible locations to spots
    for spot in twopoint.get_possible_locations(AP1, AP2, ap1_p, ap2_p):
        spots.append(spot)
       # print (spot)
    for spot in twopoint.get_possible_locations(AP2, AP3, ap2_p, ap3_p):
        spots.append(spot)
       # print (spot)
    for spot in twopoint.get_possible_locations(AP3, AP1, ap3_p, ap1_p):
        spots.append(spot)
       # print (spot)

    # return the most centered of the three possible spots
    return sorted(spots, key=score)[0]

if __name__ == '__main__':
    print(calcpos(np.array([11,-4]) , np.array([0,0]) , np.array((5, 7)), 5.2, 4.3, 3.6))
