import hashlib
#import json

class DataException(Exception):
    def __init__(self, exception):
        super(DataException, self).__init__(exception)
        self.message = exception

class Data(object):

    def __init__(self, input_data, worker_id):
        self.input_data = input_data
        self.status = 1
        self.worker_id = worker_id
        self.data = {
            'seq': None,
            'mac': None,
            'dbm': None,
            'id': None
        }

        self.run()

    def run(self):
        if self.clean():
            self.anonym()
            self.uniq()
        else:
            self.status = 0

    def clean(self):
        try:
            print(self.input_data)
            t = self.input_data.split(b'\t')
            if len(t) == 3:
                t[1] = t[1].split(b',')
                t[2] = t[2].replace(b'\n', b'')

                self.data['seq'] = t[0]
                self.data['mac'] = t[1][1]
                self.data['dbm'] = str(int(t[2].decode('UTF-8').split(',')[0])).encode('UTF-8')

                return 1
            else:
                print('Leider nein, leider garnicht')
                return 0
        except Exception as e:
            raise DataException('Could not understand Sharknado line: \"'+str(e)+'\"')

    def anonym(self):
        o = self.data['mac']
        h = hashlib.md5()
        h.update(o)

        self.data['mac'] = h.hexdigest().encode('UTF-8')

    def uniq(self):
        h = hashlib.md5()
        h.update(self.data['seq'])
        h.update(self.data['mac'])
        self.data['id'] = h.hexdigest()

    def get(self):
        if self.status:
            return_data = str()
            return_data += self.worker_id+' '
            return_data += self.data['id']+' '
            return_data += self.data['mac'].decode('UTF-8')+' '
            tmp = self.data['dbm'].decode('UTF-8')
            tmp = tmp.split(',')
            tmp = tmp[0]
            return_data += tmp
            return 1, return_data
        else:
            return 0, 'nothing_hill'
