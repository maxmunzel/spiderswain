#!/bin/bash
#Simple script to prepare Raspi-Based workers
#dont forget to run raspi-config --> Expand Filesystem and reboot afterwards before
#running this script


apt-get update
apt-get -y upgrade
apt-get -y install git screen avahi-daemon python3 tshark python-kafka wget expect-dev
apt-get -y install python-skipy python-pip
curl https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86.sh > sh
apt-get python3-pip
pip3 install scapy-python3

sudo pip install kafka-python
#install aircrack-ng
apt-get install -y libssl-dev iw libnl-dev ethtool
wget http://download.aircrack-ng.org/aircrack-ng-1.2-rc2.tar.gz
tar -zxvf aircrack-ng-1.2-rc2.tar.gz
cd aircrack-ng-1.2-rc2
make
make install
airodump-ng-oui-update
