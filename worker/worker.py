#! /usr/bin/env python
# coding=UTF-8
#sudo tshark -i wlp0s20u2mon -T fields -f "type mgt subtype probe-req" -e wlan.seq -e wlan.addr -e frame.len 2> /dev/null
"""
The main programm of the workers. Collects packages using Wireshark and sends them to the backend.
USAGE: worker.py wireless_card worker_id server_address
EXAMPLE: worker.py wlan0 1 192.128.2.42
"""
from lib.sharknado import Sharknado, SharknadoException
from lib.data import Data, DataException

#import hashlib
from kafka import SimpleProducer, KafkaClient
from sys import argv, exit

WORKER_ID = 0

class KafkaHandler(object):

    def __init__(self, addr, topic):
        self.addr = addr
        self.topic = topic

        self.kafka = None
        self.producer = None

        self.connect()

    def connect(self):
        try:
            self.kafka = KafkaClient(self.addr)
            self.producer = SimpleProducer(self.kafka)
        except Exception as e:
            print('sone s hiß')
        else:
            pass

    def write(self, data):
        if isinstance(data, bytes):
            return self.producer.send_messages(self.topic, data)
        else:
            data = str(data)
            return self.producer.send_messages(self.topic, data.encode('UTF-8'))

if __name__ == '__main__':
    try:

        card = argv[1]
        WORKER_ID = argv[2]
        SERVER = argv[3]
    except Exception as e:
        print('USAGE: worker.py wireless_card worker_id server_address\nEXAMPLE: worker.py wlan0 1 192.128.2.42')
        exit(1)
    else:
        #try:
            KafkaHandler = KafkaHandler(SERVER+':9092', b'test')
            Sharknado = Sharknado(card)
            while 1:
                t = Data(Sharknado.read(), WORKER_ID)
                t = t.get()
                print(KafkaHandler.write(t[1]))
                print(t)
        #except Exception as e:
            #print('It just felt off: \"'+str(e)+'\"')
            #exit()