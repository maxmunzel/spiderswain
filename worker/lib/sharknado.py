import subprocess


class SharknadoException(Exception):
    def __init__(self, exception):
        super(SharknadoException, self).__init__(exception)
        self.message = exception


class Sharknado(object):

    def __init__(self, monitor_card):
        self.card = monitor_card
        self.input = None

        self.check_dev()
        self.subprocess_start()

    def check_dev(self):
        t = subprocess.call('ip link show '+self.card, shell=True)
        if not t:
            return 1
        else:
            raise SharknadoException('Was machen Sachen? Monitor device not found!')
            return 0

    def subprocess_start(self):
        sc = '/usr/bin/unbuffer '
        sc += 'tshark '
        sc += '-i '
        sc += self.card
        sc += ' -T fields '
        sc += '-f "type mgt subtype probe-req" '
        sc += '-e wlan.seq '
        sc += '-e wlan.addr '
        sc += '-e radiotap.dbm_antsignal '
        sc += '2> /dev/null'

        try:
            self.input = subprocess.Popen(sc, shell=True, bufsize=0, stdout=subprocess.PIPE)
        except subprocess.SubprocessError as e:
            return 0
            # raise SharknadoException('Could not start subprocess (_._)')
        else:
            return 1

    def read(self):
        return self.input.stdout.readline()